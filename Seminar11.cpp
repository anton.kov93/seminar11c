﻿#include <iostream>
#include <string>
#include <Windows.h> 

class Player 
{
public:
    std::string name;
    int score;
};

void bubblesort(Player* l, Player* r)
{
    int sz = r - l;
    if (sz <= 1)
    {
         return; 
    }

    bool b = true;

    while (b)
    {
        b = false;

        for (Player* i = l; i + 1 < r; i++)
        {
            if (i->score < (i + 1)->score)  // Сравниваем количество очков игроков
            {
                std::swap(*i, *(i + 1));
                b = true;
            }
        }
        r--;
    }
}

int main() 
{
    //  Перевод на русский
    setlocale(LC_ALL, "Rus");
    SetConsoleOutputCP(1251);
    SetConsoleCP(1251);

    // Запрашиваем количество игроков
    int numPlayers;
    std::cout << "Введите количество игроков: ";
    std::cin >> numPlayers;

    // Создаем динамический массив объектов Player
    Player* players = new Player[numPlayers];

    // Получаем от пользователя имена игроков и их очки
    for (int i = 0; i < numPlayers; ++i) 
    {
        std::cout << "Введите имя игрока " << i + 1 << ": ";
        std::cin >> players[i].name;

        std::cout << "Введите количество очков для игрока " << players[i].name << ": ";
        std::cin >> players[i].score;
    }

    // Сортируем массив игроков по убыванию очков
    bubblesort(players, players + numPlayers);

    // Выводим информацию о каждом игроке в отсортированном виде
    std::cout << "\nИнформация о игроках (отсортировано по убыванию очков):\n";
    for (int i = 0; i < numPlayers; ++i)
    {
        std::cout << "Игрок " << i + 1 << ": " << players[i].name << ", Очки: " << players[i].score << std::endl;
    }

    // Освобождаем выделенную память
    delete[] players;

    return 0;
}